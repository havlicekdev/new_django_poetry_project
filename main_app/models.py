from django.db import models


# Create your models here.
class Employee(models.Model):
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    email = models.EmailField()

    def __str__(self):
        self.label = (f"{self.lastname} {self.firstname}  [ {self.email} ]")
        return self.label
