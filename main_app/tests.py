from django.test import TestCase
from model_bakery import baker
from pprint import pprint

from main_app.models import Employee


class TestEmployeeModel(TestCase):
    def setUp(self):
        self.Employee = baker.make(Employee)
        pprint(self.Employee.__dict__)
